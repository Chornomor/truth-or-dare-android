package com.truth_or_dare.types;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by eugenetroyanskii on 17.04.16.
 */
public class GameSession {

    private ArrayList<String> newTruth;
    private ArrayList<String> newDare;
    private ArrayList<String> oldTruth;
    private ArrayList<String> oldDare;
    private Context context;

    public GameSession(Context context) {
        this.context = context;
        initTruthList();
        initDareList();
    }

    private void initTruthList(){
        newTruth = new ArrayList<>();
        oldTruth = new ArrayList<>();
        for(int i = 0; i < Constants.TOTAL_TRUTH_EMOUNT; ++i){
            newTruth.add(Settings.getInstance().getStringFromResourse(context,"truth" + i));
        }
    }

    private void initDareList(){
        newDare = new ArrayList<>();
        oldDare = new ArrayList<>();
        for(int i = 0; i < Constants.TOTAL_DARE_EMOUNT; ++i){
            newDare.add(Settings.getInstance().getStringFromResourse(context,"dare" + i));
        }
    }

    public String getRandomTruth(){
        String truth = "";
        if(newTruth.size() > 1){
            int truthPosition = (int)(Math.random()*newTruth.size()-1);
            truth = newTruth.get(truthPosition);
            oldTruth.add(newTruth.get(truthPosition));
            newTruth.remove(truthPosition);
        }else {
            truth = newTruth.get(0);
            oldTruth.add(newTruth.get(0));
            newTruth = oldTruth;
            oldTruth = new ArrayList<>();
        }
        return truth;
    }

    public String getRandomDare(){
        String dare = "";
        if(newDare.size() > 1){
            int darePosition = (int)(Math.random()*newDare.size()-1);
            dare = newDare.get(darePosition);
            oldDare.add(newDare.get(darePosition));
            newDare.remove(darePosition);
        }else {
            dare = newDare.get(0);
            oldDare.add(newDare.get(0));
            newDare = oldDare;
            oldDare = new ArrayList<>();
        }
        return dare;
    }
}
