package com.truth_or_dare.types;

/**
 * Created by eugenetroyanskii on 27.04.16.
 */
public interface LocalizationKeys {

    String SELECT_LANGUAGE = "select_language";
    String BTN_ENGLISH = "english";
    String BTN_GERMAN = "german";
    String BTN_PLAY = "play";
    String BTN_ABOUT = "about";
    String BTN_LANGUAGE = "language";
    String BTN_BUY = "buy";
    String TITLE_ABOUT = "about_title";
    String ABOUT_INFO_1 = "about_info_1";
    String ABOUT_INFO_2 = "about_info_2";
    String ABOUT_INFO_3 = "about_info_3";
    String ABOUT_INFO_4 = "about_info_4";
    String ABOUT_INFO_5 = "about_info_5";
    String ABOUT_INFO_6 = "about_info_6";
    String ABOUT_INFO_7 = "about_info_7";
    String ABOUT_INFO_8 = "about_info_8";
    String ABOUT_INFO_9 = "about_info_9";
    String SHARED_MSG = "shared_msg";
    String SORRY_MSG = "sorry_msg";
    String MUST_BE_13 = "sorry_info";
    String ARE_YOU_13 = "are_you_aged";
    String NO_APP_WARNING = "no_app_warning";
    String BTN_CANCEL = "btn_cancel";
    String TITLE_STORE = "title_store";
    String GERMANY = "germany";
    String ERROR = "error";
    String SHARE_VIA = "share_via";

}
