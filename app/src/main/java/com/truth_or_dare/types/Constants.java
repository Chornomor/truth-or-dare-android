package com.truth_or_dare.types;

/**
 * Created by eugenetroyanskii on 19.04.16.
 */
public interface Constants {
    int TOTAL_TRUTH_EMOUNT = 118;
    int TOTAL_DARE_EMOUNT = 203;

    String TRUTH = "TRUTH";
    String DARE = "DARE";
    String IS_13 = "is13";
    String SPREF = "spref";
    String FIRST_LANGUAGE_SELECT = "first_select";
}
