package com.truth_or_dare.types;

import android.content.Context;

/**
 * Created by eugenetroyanskii on 16.04.16.
 */
public class Settings {
    private static Settings ourInstance = new Settings();

    public static final String ENGLISH_LANGUAGE = "english";
    public static final String GERMAN_LANGUAGE = "deutsch";

    public String language = "english";
    public boolean firstLanguageSelect = true;
    public boolean is13years = false;

    public static Settings getInstance() {
        if (ourInstance == null) {
            ourInstance = new Settings();
        }
        return ourInstance;
    }

    public String getStringFromResourse(Context context, String key) {
        String value = "";
        int resId = context.getResources().getIdentifier(language.charAt(0) + key, "string", context.getPackageName());
        if (resId > 0) {
            value = context.getString(resId);
        }
        return value;
    }
}
