package com.truth_or_dare.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truth_or_dare.R;

import com.truth_or_dare.dialogs.InfoDialogWithTitle;
import com.truth_or_dare.types.Constants;
import com.truth_or_dare.types.LocalizationKeys;
import com.truth_or_dare.types.Settings;

/**
 * Created by eugenetroyanskii on 29.04.16.
 */
public class ConfirmAgeActivity extends Activity implements View.OnClickListener {

    private Button btnYes, btnNo;
    private TextView tvConfirmInfo;
    private LinearLayout llBtns;

    private Typeface typefaceMittel;

    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.confirm_age_cativity);
        typefaceMittel = Typeface.createFromAsset(getAssets(), "fonts/din-mittel-schrift-std.otf");
        sp = getSharedPreferences(Constants.SPREF, Context.MODE_PRIVATE);

        btnYes = (Button)findViewById(R.id.btn_yes);
        btnNo = (Button)findViewById(R.id.btn_no);
        tvConfirmInfo = (TextView)findViewById(R.id.tv_confirm_info);
        tvConfirmInfo.setTypeface(typefaceMittel);
        tvConfirmInfo.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ARE_YOU_13));

        llBtns = (LinearLayout)findViewById(R.id.ll_buttons);
        if(Settings.getInstance().language.equals(Settings.ENGLISH_LANGUAGE)){
            llBtns.setBackground(getResources().getDrawable(R.drawable.yes_no_e));
        }else {
            llBtns.setBackground(getResources().getDrawable(R.drawable.yes_no_d));
        }
        btnYes.setOnClickListener(this);
        btnNo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_yes:
                SharedPreferences.Editor ed = sp.edit();
                ed.putBoolean(Constants.IS_13, true);
                ed.apply();
                ed.commit();
                Intent intent = new Intent(this, GameActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_no:
                Dialog dialog = new InfoDialogWithTitle(this, LocalizationKeys.MUST_BE_13 , LocalizationKeys.SORRY_MSG);
                dialog.show();
        }
    }
}