package com.truth_or_dare.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.truth_or_dare.R;

import com.truth_or_dare.dialogs.MenuDialog;
import com.truth_or_dare.dialogs.InfoDialog;
import com.truth_or_dare.dialogs.SharingDialog;
import com.truth_or_dare.types.Constants;
import com.truth_or_dare.types.GameSession;
import com.truth_or_dare.types.LocalizationKeys;
import com.truth_or_dare.types.ShakeDetector;

/**
 * Created by eugenetroyanskii on 16.04.16.
 */
public class GameActivity extends FragmentActivity implements View.OnClickListener{

    private Button btnTruth, btnDare, btnShare, btnFlash;
    private TextView truthDareDisplay;
    private LinearLayout llButtons, llFlash;

    private GameSession gameSession;
    private MenuDialog menuDialog;
    private InfoDialog sharedDialog;
    private SharingDialog sharingDialog;

    private int lastClickedBtn = 0;
    private int lastBackGround;
    private int newBackGround = R.drawable.unselected;
    private String action = "";
    public static boolean isShared = false;
    private boolean isFirstCreated = true;

    // The following are used for the shake detection
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;

    AnimationDrawable animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);

        gameSession = new GameSession(this);
        menuDialog = new MenuDialog(this);

        setupViews();
        setupTypeface();
        setupButtons();
        initShakeDetector();
    }

    private void setupViews() {
        llButtons = (LinearLayout)findViewById(R.id.ll_buttons);
        btnTruth = (Button)findViewById(R.id.btn_truth);
        btnDare = (Button)findViewById(R.id.btn_dare);
        btnShare = (Button)findViewById(R.id.btn_share);
        btnShare.setVisibility(View.INVISIBLE);
        btnFlash = (Button)findViewById(R.id.btn_dare_flash);
        btnFlash.setBackgroundResource(R.drawable.flash);
        llFlash = (LinearLayout)findViewById(R.id.ll_flash);
        truthDareDisplay = (TextView)findViewById(R.id.tv_truth_dare_display);
    }

    private void setupTypeface() {
        Typeface typefaceMittel = Typeface.createFromAsset(getAssets(), "fonts/din-mittel-schrift-std.otf");
        truthDareDisplay.setTypeface(typefaceMittel);
    }

    private void initShakeDetector() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                handleShakeEvent(count);
            }
        });
    }

    private void setupButtons() {
        btnDare.setOnClickListener(this);
        btnTruth.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnTruth.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(v.getId() == lastClickedBtn) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            pressBtn(R.drawable.truth_pressed);
                            break;
                        case MotionEvent.ACTION_UP:
                            upBtn(lastBackGround);
                            break;
                    }
                    return false;
                }else {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            pressBtn(R.drawable.truth_pressed);
                            break;
                        case MotionEvent.ACTION_UP:
                            upBtn(R.drawable.unselected);
                            break;
                    }
                    return false;
                }
            }
        });
        btnDare.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(v.getId() == lastClickedBtn){
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            pressBtn(R.drawable.dare_pressed);
                            break;
                        case MotionEvent.ACTION_UP:
                            upBtn(lastBackGround);
                            break;
                    }
                    return false;
                }else {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            pressBtn(R.drawable.dare_pressed);
                            break;
                        case MotionEvent.ACTION_UP:
                            upBtn(R.drawable.unselected);
                            break;
                    }
                    return false;
                }
            }
        });
    }

    private void startFlashAnimation(){
        btnFlash.setBackgroundResource(R.drawable.flash_animation);
        animation = (AnimationDrawable) btnFlash.getBackground();
        animation.start();
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.heartbeat_6);
        mp.start();
    }

    private void upBtn(int background){
        llButtons.setBackground(getResources().getDrawable(background));
        newBackGround = lastBackGround;
    }

    private void pressBtn(int background){
        lastBackGround = newBackGround;
        newBackGround = background;
        llButtons.setBackground(getResources().getDrawable(newBackGround));
    }

    private void handleShakeEvent(int count){
        if(count == 1){
            int random =  (int)(Math.random() * 2);
            if(random == 0){
                onClick(btnDare);
            }
            if(random == 1){
                onClick(btnTruth);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        if(isFirstCreated){
            startFlashAnimation();
            isFirstCreated = false;
        }
        isShared = false;
    }

    @Override
    public void onPause() {
        // unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        isFirstCreated = false;
        llFlash.setVisibility(View.GONE);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(isShared){
            sharedDialog = new InfoDialog(this, LocalizationKeys.SHARED_MSG);
            sharedDialog.show();
            isShared = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sharingDialog.callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.d("DEBUG", "on activity result - " + resultCode + " " + requestCode + " " + data + " ");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        isShared = false;
        switch (item.getItemId()) {
            case R.id.dots_item:
                menuDialog.setTopOffset(findViewById(R.id.dots_item).getBottom());
                menuDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        llFlash.setVisibility(View.GONE);
        switch (v.getId()){
            case R.id.btn_truth:
                lastClickedBtn = R.id.btn_truth;
                truthDareDisplay.setText(gameSession.getRandomTruth());
                truthDareDisplay.setTextColor(getResources().getColor(R.color.truthTextColor));
                llButtons.setBackground(getResources().getDrawable(R.drawable.truth_selected));
                newBackGround = R.drawable.truth_selected;
                action = Constants.TRUTH;
                btnShare.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_dare:
                lastClickedBtn = R.id.btn_dare;
                truthDareDisplay.setText(gameSession.getRandomDare());
                truthDareDisplay.setTextColor(getResources().getColor(R.color.dareTextColor));
                llButtons.setBackground(getResources().getDrawable(R.drawable.dare_selected));
                newBackGround = R.drawable.dare_selected;
                action = Constants.DARE;
                btnShare.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_share:
                String shareBody = action + " - " + truthDareDisplay.getText().toString() + " #5truthordare - http://linktoapp";
                sharingDialog = new SharingDialog(this, shareBody);
                sharingDialog.show();
                isShared = true;
                break;
        }
    }
}
