package com.truth_or_dare.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.truth_or_dare.R;
import com.truth_or_dare.types.Constants;

/**
 * Created by eugenetroyanskii on 15.04.16.
 */
public class SplashActivity extends Activity {

    SharedPreferences sp;

    private boolean is13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        sp = getSharedPreferences(Constants.SPREF, Context.MODE_PRIVATE);
        is13 = sp.getBoolean(Constants.IS_13, false);

        Thread logoTimer = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                    if(is13){
                        startActivity(new Intent(SplashActivity.this, GameActivity.class));
                    }else {
                        startActivity(new Intent(SplashActivity.this, FirstSelectLanguageActivity.class));
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };
        logoTimer.start();
    }
}