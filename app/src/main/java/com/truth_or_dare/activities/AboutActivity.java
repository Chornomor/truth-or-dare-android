package com.truth_or_dare.activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.truth_or_dare.R;

import com.truth_or_dare.dialogs.MenuDialog;
import com.truth_or_dare.types.LocalizationKeys;
import com.truth_or_dare.types.Settings;

/**
 * Created by eugenetroyanskii on 25.04.16.
 */
public class AboutActivity extends Activity {

    private MenuDialog menuDialog;
    private TextView tvTitleAbout, tvAboutInfo1, tvAboutInfo2, tvAboutInfo3,
            tvAboutInfo4, tvAboutInfo5, tvAboutInfo6, tvAboutInfo7, tvAboutInfo8, tvAboutInfo9;
    private Typeface typefaceMittel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);
        menuDialog = new MenuDialog(this);
        typefaceMittel = Typeface.createFromAsset(getAssets(), "fonts/din-mittel-schrift-std.otf");

        tvTitleAbout = (TextView)findViewById(R.id.tv_title_about);
        tvAboutInfo1 = (TextView)findViewById(R.id.tv_info_about1);
        tvAboutInfo2 = (TextView)findViewById(R.id.tv_info_about2);
        tvAboutInfo3 = (TextView)findViewById(R.id.tv_info_about3);
        tvAboutInfo4 = (TextView)findViewById(R.id.tv_info_about4);
        tvAboutInfo5 = (TextView)findViewById(R.id.tv_info_about5);
        tvAboutInfo6 = (TextView)findViewById(R.id.tv_info_about6);
        tvAboutInfo7 = (TextView)findViewById(R.id.tv_info_about7);
        tvAboutInfo8 = (TextView)findViewById(R.id.tv_info_about8);
        tvAboutInfo9 = (TextView)findViewById(R.id.tv_info_about9);
        fillUINames();
    }

    private void fillUINames(){
        tvTitleAbout.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.TITLE_ABOUT));
        tvAboutInfo1.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_1));
        tvAboutInfo2.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_2));
        tvAboutInfo3.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_3));
        tvAboutInfo4.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_4));
        tvAboutInfo5.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_5));
        tvAboutInfo6.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_6));
        tvAboutInfo7.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_7));
        tvAboutInfo8.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_8));
        tvAboutInfo9.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.ABOUT_INFO_9));
        tvTitleAbout.setTypeface(typefaceMittel);
        tvAboutInfo1.setTypeface(typefaceMittel);
        tvAboutInfo2.setTypeface(typefaceMittel);
        tvAboutInfo3.setTypeface(typefaceMittel);
        tvAboutInfo4.setTypeface(typefaceMittel);
        tvAboutInfo5.setTypeface(typefaceMittel);
        tvAboutInfo6.setTypeface(typefaceMittel);
        tvAboutInfo7.setTypeface(typefaceMittel);
        tvAboutInfo8.setTypeface(typefaceMittel);
        tvAboutInfo9.setTypeface(typefaceMittel);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dots_item:
                menuDialog.setTopOffset(findViewById(R.id.dots_item).getBottom());
                menuDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
