package com.truth_or_dare.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.truth_or_dare.R;

import com.truth_or_dare.dialogs.MenuDialog;
import com.truth_or_dare.types.LocalizationKeys;
import com.truth_or_dare.types.Settings;

/**
 * Created by eugenetroyanskii on 25.04.16.
 */
public class BuyActivity extends Activity implements View.OnClickListener {

    private MenuDialog menuDialog;
    private TextView tvSubtitle;
    private Button btnUk, btnUs, btnGerman;
    Typeface typefaceRegular, typefaceMittel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buy_activity);

        typefaceRegular = Typeface.createFromAsset(getAssets(), "fonts/din-next-for-five-regular.otf");
        typefaceMittel = Typeface.createFromAsset(getAssets(), "fonts/din-mittel-schrift-std.otf");
        menuDialog = new MenuDialog(this);

        tvSubtitle = (TextView)findViewById(R.id.tv_buy_subtitle);
        btnUk = (Button)findViewById(R.id.btn_buy_uk);
        btnUs = (Button)findViewById(R.id.btn_buy_us);
        btnGerman = (Button)findViewById(R.id.btn_buy_german);
        btnGerman.setOnClickListener(this);
        btnUs.setOnClickListener(this);
        btnUk.setOnClickListener(this);
        tvSubtitle.setTypeface(typefaceMittel);
        btnUk.setTypeface(typefaceRegular);
        btnUs.setTypeface(typefaceRegular);
        btnGerman.setTypeface(typefaceRegular);
        tvSubtitle.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.TITLE_STORE));
        btnGerman.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.GERMANY));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dots_item:
                menuDialog.setTopOffset(findViewById(R.id.dots_item).getBottom());
                menuDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        String url;
        Intent i = new Intent(Intent.ACTION_VIEW);
        switch (v.getId()){
            case R.id.btn_buy_german:
                url = "http://www.amazon.de/s/ref=sr_nr_p_89_2?fst=as%3Aoff&rh=i%3Aaps%2Ck%3A5+gum%2Cp_89%3A5+Gum%7CWrigley%27s%7CWrigley&keywords=5+gum&ie=UTF8&qid=1462542367&rnid=669059031";
                i.setData(Uri.parse(url));
                break;
            case R.id.btn_buy_uk:
                url = "https://www.amazon.co.uk/gp/search/ref=sr_nr_p_89_3?fst=as%3Aoff&rh=i%3Aaps%2Ck%3A5+gum%2Cp_89%3AWrigley%27s%7C5+Gum%7CWrigley&keywords=5+gum&ie=UTF8&qid=1462542324&rnid=1632651031";
                i.setData(Uri.parse(url));
                break;
            case R.id.btn_buy_us:
                url = "http://www.wrigleyshop.com/5-Gum-Gum/b/7326785011#.VyyfqaMrIUE";
                i.setData(Uri.parse(url));
                break;
        }
        startActivity(i);
    }
}
