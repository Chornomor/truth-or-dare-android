package com.truth_or_dare.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.truth_or_dare.R;

import com.truth_or_dare.dialogs.MenuDialog;
import com.truth_or_dare.types.Constants;
import com.truth_or_dare.types.LocalizationKeys;
import com.truth_or_dare.types.Settings;

/**
 * Created by eugenetroyanskii on 23.12.16.
 */

public class FirstSelectLanguageActivity extends Activity implements View.OnClickListener {

    Button btnEnglish, btnGerman;
    private MenuDialog menuDialog;
    private TextView tvSelectLanguage;

    Typeface typefaceRegular, typefaceMittel;

    SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_language_activity);
        typefaceRegular = Typeface.createFromAsset(getAssets(), "fonts/din-next-for-five-regular.otf");
        typefaceMittel = Typeface.createFromAsset(getAssets(), "fonts/din-mittel-schrift-std.otf");
        sp = getSharedPreferences(Constants.SPREF, Context.MODE_PRIVATE);

        menuDialog = new MenuDialog(this);
        tvSelectLanguage = (TextView) findViewById(R.id.tv_select_language);
        btnEnglish = (Button) findViewById(R.id.btn_english);
        btnEnglish.setOnClickListener(this);
        btnGerman = (Button) findViewById(R.id.btn_german);
        btnGerman.setOnClickListener(this);
        tvSelectLanguage.setTypeface(typefaceMittel);
        btnEnglish.setTypeface(typefaceRegular);
        btnGerman.setTypeface(typefaceRegular);
        fillUINames();
    }


    private void fillUINames() {
        tvSelectLanguage.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.SELECT_LANGUAGE));
        btnEnglish.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.BTN_ENGLISH));
        btnGerman.setText(Settings.getInstance().getStringFromResourse(this, LocalizationKeys.BTN_GERMAN));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dots_item:
                menuDialog.setTopOffset(findViewById(R.id.dots_item).getBottom());
                menuDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_english:
                Settings.getInstance().language = Settings.ENGLISH_LANGUAGE;
                break;
            case R.id.btn_german:
                Settings.getInstance().language = Settings.GERMAN_LANGUAGE;
                break;
        }
        intent = new Intent(this, ConfirmAgeActivity.class);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean(Constants.FIRST_LANGUAGE_SELECT, true);
        ed.apply();
        ed.commit();
        startActivity(intent);
    }
}