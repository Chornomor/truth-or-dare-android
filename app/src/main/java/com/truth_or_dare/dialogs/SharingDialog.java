package com.truth_or_dare.dialogs;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.truth_or_dare.R;

import com.truth_or_dare.activities.GameActivity;
import com.truth_or_dare.types.LocalizationKeys;
import com.truth_or_dare.types.Settings;

import java.awt.font.TextAttribute;

/**
 * Created by eugenetroyanskii on 29.04.16.
 */
public class SharingDialog extends Dialog implements View.OnClickListener {

    private String shareBody;
    private String tod_title = "Truth or Dare";
    private String  facebook = "com.facebook.katana";
    private String twitter = "com.twitter.android";
    private String whatssup = "com.whatsapp";

    private Activity activity;
    private FrameLayout sharingDialog;
    private TextView tvShareVia;
    private View v;
    private Button btnFacebook, btnTwitter, btnWhatssup, btnOther, btnCancel;

    public CallbackManager callbackManager;
    public ShareDialog shareDialog;

    public SharingDialog(Activity activity, String shareBody) {
        super(activity, R.style.MyDialog);
        this.activity = activity;
        this.shareBody = shareBody;

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.dialog_for_sharing, null);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);
        setContentView(v);

        tvShareVia = (TextView) v.findViewById(R.id.tv_share_via);
        btnFacebook = (Button) v.findViewById(R.id.btn_facebook);
        btnTwitter = (Button) v.findViewById(R.id.btn_twitter);
        btnWhatssup = (Button) v.findViewById(R.id.btn_whatssup);
        btnOther = (Button) v.findViewById(R.id.btn_other);
        btnCancel = (Button) v.findViewById(R.id.btn_cancel);
        btnCancel.setText(Settings.getInstance().getStringFromResourse(activity, LocalizationKeys.BTN_CANCEL));
        tvShareVia.setText(Settings.getInstance().getStringFromResourse(activity, LocalizationKeys.SHARE_VIA));
        btnTwitter.setOnClickListener(this);
        btnWhatssup.setOnClickListener(this);
        btnFacebook.setOnClickListener(this);
        btnOther.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        sharingDialog = (FrameLayout)findViewById(R.id.sharing_dialog);
        sharingDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        if(btnId == R.id.btn_facebook){
            if (isAppInstalled(activity, facebook)) {
                FacebookSdk.sdkInitialize(activity.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(activity);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        InfoDialog sharedDialog = new InfoDialog(activity, LocalizationKeys.SHARED_MSG);
                        sharedDialog.show();
                        GameActivity.isShared = false;
                    }

                    @Override
                    public void onCancel() {
                        GameActivity.isShared = false;
                        dismiss();
                    }

                    @Override
                    public void onError(FacebookException e) {
                        GameActivity.isShared = false;
                        InfoDialog sharedDialog = new InfoDialog(activity, LocalizationKeys.ERROR);
                        sharedDialog.show();
                    }
                });

                ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("http://harmonypark.net/"))
                        .setContentTitle(tod_title)
                        .setContentDescription(shareBody)
                        .build();
                shareDialog.show(content);
            }else {
                Dialog d = new InfoDialog(activity, LocalizationKeys.NO_APP_WARNING);
                d.show();
                dismiss();
            }
        }
        if(btnId == R.id.btn_twitter) {
            if(isAppInstalled(activity, twitter)){
                Intent shareIntent = new Intent();
                shareIntent.setType("text/plain");
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage(twitter);

                shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, tod_title);
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                GameActivity.isShared = true;
                activity.startActivity(shareIntent);

            }else {
                Dialog d = new InfoDialog(activity, LocalizationKeys.NO_APP_WARNING);
                d.show();
                dismiss();
            }
        }
        if(btnId == R.id.btn_whatssup){
            if(isAppInstalled(activity, whatssup)){
                Intent shareIntent = new Intent();
                shareIntent.setType("text/plain");
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage(whatssup);

                shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, tod_title);
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                GameActivity.isShared = true;
                activity.startActivity(shareIntent);
            }else {
                Dialog d = new InfoDialog(activity, LocalizationKeys.NO_APP_WARNING);
                d.show();
                dismiss();
            }
        }
        if(btnId == R.id.btn_other){
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, tod_title);
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            activity.startActivity(Intent.createChooser(sharingIntent, Settings.getInstance().getStringFromResourse(activity, LocalizationKeys.SHARE_VIA)));
        }
        if(btnId == R.id.btn_cancel){
            dismiss();
        }
    }

    private boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
