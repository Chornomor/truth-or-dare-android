package com.truth_or_dare.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.truth_or_dare.R;

import com.truth_or_dare.activities.AboutActivity;
import com.truth_or_dare.activities.BuyActivity;
import com.truth_or_dare.activities.GameActivity;
import com.truth_or_dare.activities.SelectLanguageActivity;
import com.truth_or_dare.types.LocalizationKeys;
import com.truth_or_dare.types.Settings;

/**
 * Created by eugenetroyanskii on 18.04.16.
 */
public class MenuDialog extends Dialog implements View.OnClickListener {
    Context _context;
    Button btnPlay, btnAbout, btnLanguage, buy;
    RelativeLayout btnBuy;
    Typeface typefaceRegular;

    public MenuDialog(Context context) {
        super(context, R.style.PopupDialog);
        _context = context;
        typefaceRegular = Typeface.createFromAsset(_context.getAssets(), "fonts/din-next-for-five-regular.otf");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_menu);
        fillUINames();
        getWindow().setGravity(Gravity.TOP);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        btnPlay.setOnClickListener(this);
        btnAbout.setOnClickListener(this);
        btnLanguage.setOnClickListener(this);
        btnBuy.setOnClickListener(this);


    }

    private void fillUINames(){
        btnPlay = (Button)findViewById(R.id.btn_play);
        btnAbout = (Button)findViewById(R.id.btn_about);
        btnLanguage = (Button)findViewById(R.id.btn_language);
        btnBuy = (RelativeLayout)findViewById(R.id.btn_buy);
        buy = (Button)findViewById(R.id.buy);

        btnPlay.setTypeface(typefaceRegular);
        btnAbout.setTypeface(typefaceRegular);
        btnLanguage.setTypeface(typefaceRegular);
        buy.setTypeface(typefaceRegular);

        btnPlay.setText(Settings.getInstance().getStringFromResourse(_context, LocalizationKeys.BTN_PLAY));
        btnAbout.setText(Settings.getInstance().getStringFromResourse(_context, LocalizationKeys.BTN_ABOUT));
        btnLanguage.setText(Settings.getInstance().getStringFromResourse(_context, LocalizationKeys.BTN_LANGUAGE));
        buy.setText(Settings.getInstance().getStringFromResourse(_context, LocalizationKeys.BTN_BUY));
    }

    public void setTopOffset(int offset) {
        getWindow().getAttributes().y = offset;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        String className = "";
        switch (v.getId()) {
            case R.id.btn_play:
                className = GameActivity.class.getName();
                intent = new Intent(_context, GameActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case R.id.btn_about:
                className = AboutActivity.class.getName();
                intent = new Intent(_context, AboutActivity.class);
                break;
            case R.id.btn_language:
                className = SelectLanguageActivity.class.getName();
                intent = new Intent(_context, SelectLanguageActivity.class);
                break;
            case R.id.btn_buy:
                className = BuyActivity.class.getName();
                intent = new Intent(_context, BuyActivity.class);
                break;
        }
        if (!className.equals(_context.getClass().getName())) {
            ((Activity)_context).startActivity(intent);
        }
        dismiss();
    }
}