package com.truth_or_dare.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.truth_or_dare.R;

import com.truth_or_dare.types.Settings;

/**
 * Created by eugenetroyanskii on 29.04.16.
 */
public class InfoDialogWithTitle extends Dialog {

    private Context _context;
    private Typeface typefaceRegular, typefaceMittel;
    private RelativeLayout sharedDialog;
    private TextView tvTitle, tvInfo;

    public InfoDialogWithTitle(Context context, String messageKey, String titleKey) {
        super(context, R.style.PopupDialog);
        _context = context;
        typefaceRegular = Typeface.createFromAsset(_context.getAssets(), "fonts/din-next-for-five-regular.otf");
        typefaceMittel = Typeface.createFromAsset(_context.getAssets(), "fonts/din-mittel-schrift-std.otf");


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_info_with_title);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tvTitle = (TextView) findViewById(R.id.tv_title_info_with_title);
        tvTitle.setTypeface(typefaceRegular);
        tvTitle.setText(Settings.getInstance().getStringFromResourse(_context, titleKey));
        tvInfo = (TextView) findViewById(R.id.tv_info_with_title);
        tvInfo.setTypeface(typefaceMittel);
        tvInfo.setText(Settings.getInstance().getStringFromResourse(_context, messageKey));
        sharedDialog = (RelativeLayout) findViewById(R.id.shared_dialog);
        sharedDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
