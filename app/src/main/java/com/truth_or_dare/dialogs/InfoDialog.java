package com.truth_or_dare.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.truth_or_dare.R;

import com.truth_or_dare.types.Settings;

/**
 * Created by eugenetroyanskii on 26.04.16.
 */
public class InfoDialog extends Dialog {

    private Context _context;
    private Typeface typefaceRegular;
    private RelativeLayout sharedDialog;
    private TextView tvShared;

    public InfoDialog(Context context, String messageKey) {
        super(context, R.style.PopupDialog);
        _context = context;
        typefaceRegular = Typeface.createFromAsset(_context.getAssets(), "fonts/din-next-for-five-regular.otf");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_shared);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tvShared = (TextView)findViewById(R.id.tv_shared);
        tvShared.setTypeface(typefaceRegular);
        tvShared.setText(Settings.getInstance().getStringFromResourse(_context, messageKey));
        sharedDialog = (RelativeLayout)findViewById(R.id.shared_dialog);
        sharedDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
